import os
import pickle

import bilby
import lal
import lalsimulation
import numpy as np
from astropy.constants import *
from bilby.core.utils import ra_dec_to_theta_phi, speed_of_light

randomseed = 543
nlive = 4000
flow = 5
srate = 4096
fref = 100
tc_offset = 1
sample_CE_arrival_time = True
outdir = "results"

np.random.seed(randomseed)

if not os.path.exists(outdir):
    os.mkdir(outdir)


msun_to_seconds = lal.G_SI * lal.MSUN_SI / lal.C_SI**3.0
m1 = 1.44
m2 = 1.4
ra = 0.78
dec = 1.16
theta_jn = 1.0
psi = 1.57
phase = 1.57
geocent_time = 630696093
luminosity_distance = 35
lt = 300
dlt = -100


def tau_up_to_2PN(f, m1, m2, chi1, chi2):
    """Taken from Eq. (3.3) of arXiv:gr-qc/9502040"""
    # mass combinations
    mtot = m1 + m2
    mtot_in_seconds = mtot * msun_to_seconds
    mc = (m1 * m2) ** (3.0 / 5.0) / (m1 + m2) ** (1.0 / 5.0)
    mc_in_seconds = mc * msun_to_seconds
    eta = m1 * m2 / (m1 + m2) ** 2.0
    x = np.pi * mtot_in_seconds * f
    # spin combinations
    beta = (
        (113.0 * (m1 / mtot) ** 2.0 + 75.0 * eta) * chi1
        + (113.0 * (m2 / mtot) ** 2.0 + 75.0 * eta) * chi2
    ) / 12.0
    sigma = (-247.0 * chi1 * chi2 + 721.0 * chi1 * chi2) * eta / 48.0
    # up to 2PN
    tau0 = 5.0 / 256.0 * mc_in_seconds * (np.pi * mc_in_seconds * f) ** (-8.0 / 3.0)
    tau2 = 4.0 / 3.0 * (743.0 / 336.0 + 11.0 * eta / 4.0) * x ** (2.0 / 3.0) * tau0
    tau3 = -8.0 / 5.0 * (4.0 * np.pi - beta) * x * tau0
    tau4 = (
        2.0
        * (
            3058673.0 / 1016064.0
            + 5429.0 * eta / 1008.0
            + 617.0 * eta**2.0 / 144.0
            - sigma
        )
        * x ** (4.0 / 3.0)
        * tau0
    )
    return tau0 + tau2 + tau3 + tau4


def finite_size_factor(x, y):
    return 0.5 * (
        np.exp(-np.pi * 1j * x * (1.0 + y)) * np.sinc(x * (1 - y))
        + np.exp(np.pi * 1j * x * (1.0 - y)) * np.sinc(x * (1 + y))
    )


def calculate_signal(
    interferometer,
    waveform_generator,
    injection_parameters,
    start_time,
    minimum_frequency,
    earth_rotation_time_delay=True,
    earth_rotation_beam_patterns=True,
    finite_size=True,
):
    (
        converted_injection_parameters,
        _,
    ) = bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters(
        injection_parameters
    )
    waveform_polarizations = waveform_generator.frequency_domain_strain(
        converted_injection_parameters
    )
    frequencies = waveform_generator.frequency_array
    idxs_above_minimum_frequency = frequencies > (
        minimum_frequency - (frequencies[1] - frequencies[0])
    )
    tc = converted_injection_parameters["geocent_time"]

    # get sidereal time at each frequency
    if earth_rotation_time_delay or earth_rotation_beam_patterns:
        m1, m2 = (
            converted_injection_parameters["mass_1"],
            converted_injection_parameters["mass_2"],
        )
        chi1 = converted_injection_parameters["a_1"] * np.cos(
            converted_injection_parameters["tilt_1"]
        )
        chi2 = converted_injection_parameters["a_2"] * np.cos(
            converted_injection_parameters["tilt_2"]
        )
        times_from_tc = -tau_up_to_2PN(
            frequencies[idxs_above_minimum_frequency], m1, m2, chi1, chi2
        )
        gmst_at_tc = lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(tc))
        day = 24.0 * 60.0 * 60.0
        gmst_day_after = lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(tc + day))
        one_second_to_gmst = (gmst_day_after - gmst_at_tc) / day
        gmsts = gmst_at_tc + one_second_to_gmst * times_from_tc

    else:
        gmsts = np.ones(
            sum(idxs_above_minimum_frequency)
        ) * lal.GreenwichMeanSiderealTime(lal.LIGOTimeGPS(tc))

    # calculate basis vectors of GW frame
    ra = converted_injection_parameters["ra"]
    dec = converted_injection_parameters["dec"]
    psi = converted_injection_parameters["psi"]
    thetas, phis = ra_dec_to_theta_phi(ra, dec, gmsts)
    cosphis = np.cos(phis)
    costhetas = np.cos(thetas)
    sinphis = np.sin(phis)
    sinthetas = np.sin(thetas)
    u = np.zeros(shape=(3, len(phis)))
    u[0] = cosphis * costhetas
    u[1] = costhetas * sinphis
    u[2] = -sinthetas
    v = np.zeros(shape=(3, len(phis)))
    v[0] = -sinphis
    v[1] = cosphis
    m = -u * np.sin(psi) - v * np.cos(psi)
    n = -u * np.cos(psi) + v * np.sin(psi)
    omegas = np.zeros(shape=(3, len(phis)))
    omegas[0] = sinthetas * cosphis
    omegas[1] = sinthetas * sinphis
    omegas[2] = costhetas

    # beam patterns
    pol_plus = np.einsum("ik,jk->ijk", m, m) - np.einsum("ik,jk->ijk", n, n)
    pol_cross = np.einsum("ik,jk->ijk", m, n) + np.einsum("ik,jk->ijk", n, m)

    if not finite_size:
        fps = np.einsum("ij,ijk->k", interferometer.geometry.detector_tensor, pol_plus)
        fcs = np.einsum("ij,ijk->k", interferometer.geometry.detector_tensor, pol_cross)
        if not earth_rotation_beam_patterns:
            fps = fps[-1] * np.ones(len(fps))
            fcs = fcs[-1] * np.ones(len(fcs))
    else:
        pol_plus = np.einsum("ik,jk->ijk", m, m) - np.einsum("ik,jk->ijk", n, n)
        pol_cross = np.einsum("ik,jk->ijk", m, n) + np.einsum("ik,jk->ijk", n, m)
        detector_tensor_xx = 0.5 * np.einsum(
            "i,j->ij", interferometer.geometry.x, interferometer.geometry.x
        )
        detector_tensor_yy = 0.5 * np.einsum(
            "i,j->ij", interferometer.geometry.y, interferometer.geometry.y
        )
        fpxx = np.einsum("ij,ijk->k", detector_tensor_xx, pol_plus)
        fpyy = np.einsum("ij,ijk->k", detector_tensor_yy, pol_plus)
        fcxx = np.einsum("ij,ijk->k", detector_tensor_xx, pol_cross)
        fcyy = np.einsum("ij,ijk->k", detector_tensor_yy, pol_cross)
        if not earth_rotation_beam_patterns:
            fpxx = fpxx[-1] * np.ones(len(fpxx))
            fpyy = fpyy[-1] * np.ones(len(fpyy))
            fcxx = fcxx[-1] * np.ones(len(fcxx))
            fcyy = fcyy[-1] * np.ones(len(fcyy))

        px = -np.dot(omegas.T, interferometer.geometry.x)
        py = -np.dot(omegas.T, interferometer.geometry.y)
        fL_over_c = (
            frequencies[idxs_above_minimum_frequency]
            * interferometer.geometry.length
            * 10.0**3.0
            / speed_of_light
        )
        Dxx = finite_size_factor(fL_over_c, px)
        Dyy = finite_size_factor(fL_over_c, py)
        fps = fpxx * Dxx - fpyy * Dyy
        fcs = fcxx * Dxx - fcyy * Dyy

    # time-shift factor
    dts = -np.dot(omegas.T, interferometer.geometry.vertex) / speed_of_light
    ifo_times = tc - start_time + dts
    if not earth_rotation_time_delay:
        ifo_times = ifo_times[-1]

    # multiply them
    h = np.zeros(len(frequencies), dtype=complex)
    h[idxs_above_minimum_frequency] = (
        fps * waveform_polarizations["plus"][idxs_above_minimum_frequency]
        + fcs * waveform_polarizations["cross"][idxs_above_minimum_frequency]
    )
    h[idxs_above_minimum_frequency] *= np.exp(
        -1j * 2.0 * np.pi * frequencies[idxs_above_minimum_frequency] * ifo_times
    )

    return frequencies, h


z = bilby.gw.conversion.luminosity_distance_to_redshift(luminosity_distance)
q = bilby.gw.conversion.component_masses_to_mass_ratio(m1, m2)
mcz = bilby.gw.conversion.component_masses_to_chirp_mass(m1, m2) * (1 + z)
Interferometers = ["CE"]

injection_parameters = dict(
    chirp_mass=mcz,
    mass_ratio=q,
    chi_1=0.0,
    chi_2=0.0,
    lambda_tilde=lt,
    delta_lambda_tilde=dlt,
    ra=ra,
    dec=dec,
    luminosity_distance=luminosity_distance,
    theta_jn=theta_jn,
    psi=psi,
    phase=phase,
    geocent_time=geocent_time,
)

t0 = (
    -(5.0 / 256.0)
    * mcz
    * GM_sun.value
    / c.value**3
    * (np.pi * mcz * GM_sun.value / c.value**3 * flow) ** (-8.0 / 3.0)
)

seglen = 2 ** (np.int32(np.log2(np.abs(t0))) + 1)
start_time = injection_parameters["geocent_time"] - seglen + tc_offset
waveformname = "TaylorF2"

# costruct ifos
ifos = bilby.gw.detector.InterferometerList(Interferometers)
waveform_generator = bilby.gw.WaveformGenerator(
    duration=seglen,
    sampling_frequency=srate,
    frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
    parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
    waveform_arguments=dict(
        waveform_approximant=waveformname,
        reference_frequency=fref,
        minimum_frequency=flow,
    ),
)

# print out snr
snr = 0.0
for ifo in ifos:
    frequency_array, frequency_domain_strain = calculate_signal(
        ifo,
        waveform_generator,
        injection_parameters,
        start_time,
        flow,
        earth_rotation_time_delay=True,
        earth_rotation_beam_patterns=True,
        finite_size=True,
    )
    ifo.set_strain_data_from_frequency_domain_strain(
        frequency_domain_strain, start_time=start_time, frequency_array=frequency_array
    )
    ifo.minimum_frequency = flow
    snr += ifo.optimal_snr_squared(signal=ifo.frequency_domain_strain)
snr = np.sqrt(snr.real)
print(f"The injected SNR is {snr}.")

# construct priors
priors = bilby.gw.prior.BNSPriorDict()
for key in ["mass_1", "mass_2", "lambda_1", "lambda_2"]:
    priors.pop(key)

mcmin, mcmax = (
    injection_parameters["chirp_mass"] - 1e-5,
    injection_parameters["chirp_mass"] + 1e-5,
)
priors["chirp_mass"] = bilby.core.prior.Uniform(
    name="chirp_mass", minimum=mcmin, maximum=mcmax
)
priors["mass_ratio"] = bilby.core.prior.Uniform(
    name="mass_ratio", minimum=0.125, maximum=1
)
priors["chi_1"] = 0
priors["chi_2"] = 0
priors["luminosity_distance"] = bilby.core.prior.Uniform(
    name="luminosity_distance",
    minimum=max(1, injection_parameters["luminosity_distance"] - 2000),
    maximum=max(
        injection_parameters["luminosity_distance"] + 2000,
        3 * injection_parameters["luminosity_distance"],
    ),
    unit="Mpc",
    latex_label="$d_L$",
)
priors["lambda_tilde"] = bilby.core.prior.Uniform(
    name="lambda_tilde",
    minimum=max(0, injection_parameters["lambda_tilde"] - 100),
    maximum=injection_parameters["lambda_tilde"] + 100,
)
priors["delta_lambda_tilde"] = bilby.core.prior.Uniform(
    injection_parameters["delta_lambda_tilde"] - 5000,
    injection_parameters["delta_lambda_tilde"] + 5000,
    name="delta_lambda_tilde",
)

if sample_CE_arrival_time:
    reference_ifo = bilby.gw.detector.get_empty_interferometer("CE")
    injection_parameters["CE_time"] = injection_parameters[
        "geocent_time"
    ] + reference_ifo.time_delay_from_geocenter(
        ra=injection_parameters["ra"],
        dec=injection_parameters["dec"],
        time=injection_parameters["geocent_time"],
    )
    priors["CE_time"] = bilby.core.prior.Uniform(
        minimum=injection_parameters["CE_time"] - 0.01,
        maximum=injection_parameters["CE_time"] + 0.01,
        name="CE_time",
        latex_label="$t_{CE}$",
        unit="$s$",
    )
else:
    priors["geocent_time"] = bilby.core.prior.Uniform(
        minimum=injection_parameters["geocent_time"] - 0.1,
        maximum=injection_parameters["geocent_time"] + 0.1,
        name="geocent_time",
        latex_label="$t_c$",
        unit="$s$",
    )


# set up likelihood
distance_marginalization = False
phase_marginalization = True
path_to_likelihood = os.path.join(outdir, f"likelihood.pickle")
if not os.path.exists(path_to_likelihood):
    search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
        duration=seglen,
        sampling_frequency=srate,
        frequency_domain_source_model=bilby.gw.source.binary_neutron_star_frequency_sequence,
        waveform_arguments=dict(
            waveform_approximant=waveformname, reference_frequency=fref
        ),
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
    )
    if sample_CE_arrival_time:
        time_reference = "CE"
    else:
        time_reference = "geocent"

    likelihood = bilby.gw.likelihood.MBGravitationalWaveTransientThirdGeneration(
        interferometers=ifos,
        waveform_generator=search_waveform_generator,
        priors=priors,
        reference_chirp_mass=mcmin,
        linear_interpolation=True,
        distance_marginalization=distance_marginalization,
        phase_marginalization=phase_marginalization,
        time_reference=time_reference,
        earth_rotation_time_delay=True,
        earth_rotation_beam_patterns=True,
        finite_size=True,
    )
    pickle.dump(likelihood, open(path_to_likelihood, "wb"))
else:
    likelihood = pickle.load(open(path_to_likelihood, "rb"))
    if distance_marginalization:
        priors["luminosity_distance"] = float(
            priors["luminosity_distance"].rescale(0.5)
        )
    if phase_marginalization:
        priors["phase"] = 0.0

# sampling
result = bilby.run_sampler(
    likelihood=likelihood,
    priors=priors,
    sampler="dynesty",
    use_ratio=True,
    nlive=500,
    walks=100,
    maxmcmc=5000,
    nact=10,
    npool=16,
    injection_parameters=injection_parameters,
    outdir=outdir,
    label=None,
    dlogz=0.01,
    sample="acceptance-walk",
)

# Make a corner plot.
result.plot_corner()
